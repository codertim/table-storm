
Table Storm
---------------------

This is a table viewer based on Rails and AJAX

Potential colors (via colorschemmedesigner.com:
  #7FFFF24 base green
  #4CA60C  dark green
  #FF6824  orange
  #D5FF24  yellow
  #24FFAC  light blue
