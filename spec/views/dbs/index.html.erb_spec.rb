require 'spec_helper'

describe "dbs/index.html.erb" do
  it "displays database names" do
    assign(:databases, ["employees", "clients", "guests"])
    render
    rendered.should =~ /employees/
    rendered.should =~ /clients/
    rendered.should =~ /guests/
    rendered.should_not =~ /unknown_db/
  end
end
