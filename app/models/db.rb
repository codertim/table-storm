class Db < ActiveRecord::Base
  attr_accessible :host, :name, :password, :type, :username
  DB_TYPES = {"postgres" => "pg"}
  has_many :db_tables
  has_many :recent_queries

  establish_connection :development 
end

