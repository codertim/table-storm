class DbTablesController < ApplicationController

  def show
    @db = Db.find(params[:db_id])
    logger.debug("### DbTableController#index - db name = #{@db.name} ")

    @table_name = params[:table_name]
    logger.debug("### DbTableController#index - table name = #{@table_name} ")

    @col_name = params[:col_name]
    logger.debug("### DbTableController#index - col name = #{@col_name} ")

    columns = get_columns(@db, @table_name)
    @column_object = columns.detect{ |col| col.name == @col_name}
  end


  def index
    logger.debug("### DbTableController#index - Starting ...")

    @db = Db.find(params[:db_id])
    logger.debug("### DbTableController#index - db name = #{@db.name} ")

    @table_name = params[:table_name]
    logger.debug("### DbTableController#index - table name = #{@table_name} ")

    index_action = params[:index_action] || "none"
    logger.debug("### DbTableController#index - index_action = #{index_action} ")

    @db.recent_queries << RecentQuery.create!(:table_name => @table_name)

    if index_action == "show_data"
      # show actual table values
      @table_rows = get_table_rows(@db, @table_name)
      render "index_data"
    else
      # show just column meta data for now
      @columns = get_columns(@db, @table_name)
    end
  end



  # ajax
  def lookup_field_max
      logger.debug("lookup_field_ma - Starting ...")
      db = Db.find(params[:db_id])
      @col_max = get_field_max(db, params[:table_name], params[:col_name])
      render :lookup_field_max, :layout => false
  end



  def get_field_max(db, table_name, col_name)
    max_col_value = nil
    db_adapter    = "postgresql"   # TODO: make this dynamic
    db_host       = db.host    # TODO: make this dynamic
    db_name       = db.name
    db_username   = db.username
    db_password   = db.password

    active_record_class_name = "#{table_name}".singularize.camelize

    dynamic_class = Class.new(ActiveRecord::Base) do
      self.table_name = table_name
    end
    Kernel.const_set active_record_class_name, dynamic_class

    # db connection
    begin
      conn_est    = dynamic_class.establish_connection(:adapter  => db_adapter, :host => db_host, :username => db_username, :password => db_password, :database => db_name)
      conn        = conn_est.connection   # actually check if connection works
      class_object = Kernel.const_get(active_record_class_name)
      max_col_value = class_object.send(:maximum, col_name) 
      logger.debug("get_field_max - max_col_value = #{max_col_value} ")
    rescue => conn_error
      does_connect = false
      logger.error("get_field_max - ERROR - Message: #{conn_error.message}")
      logger.error("get_field_max - ERROR - Backtrace: #{conn_error.backtrace}")
    end

    max_col_value
  end



  def get_table_rows(db, table_name)
    table_rows = nil
    db_adapter  = "postgresql"   # TODO: make this dynamic
    db_host     = db.host    # TODO: make this dynamic
    db_name     = db.name
    db_username = db.username
    db_password = db.password

    active_record_class_name = "#{table_name}".singularize.camelize
    logger.debug("##### DbTablesController#get_column_names - active_record_class_name = #{active_record_class_name} ")
    dynamic_class = Class.new(ActiveRecord::Base) do
      # self.abstract_class = true
      set_table_name table_name
    end
    Kernel.const_set active_record_class_name, dynamic_class

    # db connection
    begin
      conn_est    = dynamic_class.establish_connection(:adapter  => db_adapter, :host => db_host, :username => db_username, :password => db_password, :database => db_name)
      conn        = conn_est.connection   # actually check if connection works
      class_object = Kernel.const_get(active_record_class_name)
      logger.debug("get_table_rows - class_object = #{class_object.inspect} ")
      columns_hash = class_object.send(:column_names), class_object.send(:columns_hash)
      logger.debug("get_table_rows - columns_hash = #{columns_hash.inspect} ")
      first_column = columns_hash.first.first
      logger.debug("get_table_rows - first_column = #{first_column} ")
      # paginated_rows_array = Kaminari.paginate_array(class_object.send(:order, first_column))
      ### paginated_rows = class_object.send(:order, first_column)
      ### logger.debug("get_table_rows - paginated_rows.class = #{paginated_rows.class} ")
      ### table_rows = paginated_rows.page(params[:page]).per(5)
      table_rows = class_object.send(:order, first_column).page(params[:page]).per(5)
      # logger.debug("get_table_rows - table_rows = #{table_rows.inspect} ")
      # logger.debug("get_table_rows - table_rows.class = #{table_rows.class} ")
      # logger.debug("get_table_rows - table_rows[0] = #{table_rows[0].inspect} ")
      # logger.debug("get_table_rows - table_rows[0].class = #{table_rows[0].class} ")
      logger.debug("get_table_rows - Connection seems to be ok")
    rescue => conn_error
      does_connect = false
      logger.error("get_table_rows - ERROR - Message: #{conn_error.message}")
      logger.error("get_table_rows - ERROR - Backtrace: #{conn_error.backtrace}")
    end

    table_rows
  end



  def get_columns(db, table_name)
    db_adapter  = "postgresql"   # TODO: make this dynamic
    db_host     = db.host    # TODO: make this dynamic
    db_name     = db.name
    db_username = db.username
    db_password = db.password
    table_names = []
    columns = []

    active_record_class_name = "#{table_name}".singularize.camelize
    logger.debug("##### DbTablesController#get_column_names - active_record_class_name = #{active_record_class_name} ")
    dynamic_class = Class.new(ActiveRecord::Base) do
      # self.abstract_class = true
      set_table_name table_name   # some table names may not be pluralized, so set explicitly
    end
    Kernel.const_set active_record_class_name, dynamic_class


    # db connection
    begin
      conn_est    = dynamic_class.establish_connection(:adapter  => db_adapter, :host => db_host, :username => db_username, :password => db_password, :database => db_name)
      conn        = conn_est.connection   # actually check if connection works
      class_object = Kernel.const_get(active_record_class_name)
      logger.debug("get_column_names - class_object = #{class_object.inspect} ")
      # class_object.send(:column_names), class_object.send(:columns_hash)
      columns = class_object.send(:columns)
      logger.debug("get_column_names - columns= #{columns.inspect} ")
      logger.debug("get_column_names - Connection seems to be ok")
    rescue => conn_error
      does_connect = false
      logger.error("get_column_names - ERROR - Message: #{conn_error.message}")
      logger.error("get_column_names - ERROR - Backtrace: #{conn_error.backtrace}")
    end

    columns
  end
end

