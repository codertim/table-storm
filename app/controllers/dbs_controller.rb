class DbsController < ApplicationController

  def index
    logger.debug("index - request.url  = #{request.url} ")
    logger.debug("index - request.path = #{request.path} ")
    @dbs = Db.all
  end



  def show
    @db = Db.find(params[:id])
    @table_names = get_table_names(@db)
  end



  def edit
  end



  def new
  end



  def create
    can_connect = false

    # first see if can connect
    begin
      logger.debug("create - testing connection ...")
      can_connect = connects(params[:db_type], params[:db_name], params[:db_username], params[:db_password], params[:db_host])
      logger.debug("create - can_connect = #{can_connect}")
    rescue => err
      logger.error("create - ERROR - Message: #{err.message}")
      logger.error("create - ERROR - Backtrace: #{err.backtrace}")
    end


    if can_connect && Db.create(:type => params[:db_type], :name => params[:db_name], :username => params[:db_username], :password => params[:db_password], :host => params[:db_host])
      logger.debug("create - Created new database entry")
      redirect_to dbs_index_url
    else
      logger.error("create - ERROR - Problem creating database entry") 
      # flash.now['notice'] = "Problem verifying database connection"
      redirect_to :back, :notice => "Problem verifying database connection"
    end
  end


  def connects(db_type, db_name, db_username, db_password, db_host)
    logger.debug("connects - Starting ...")
    does_connect = false

    # create new active record subclass based on database name
    # e.g. user enters db name "shipping", class is created called "ExternalShipping"
    active_record_class_name = "External_#{db_name}".camelize
    logger.debug("connects - new class name = #{active_record_class_name}")
    dynamic_class = Class.new(ActiveRecord::Base) do
        self.abstract_class = true
    end
    Kernel.const_set active_record_class_name, dynamic_class

    logger.debug("connects - trying connection ...")
    begin
      conn_est = dynamic_class.establish_connection(:adapter  => 'postgresql', :host => db_host, :username => db_username, :password => db_password, :database => db_name)
      conn = conn_est.connection   # actually check if connection works
      does_connect = true
    rescue => conn_error
      does_connect = false
      logger.error("connects - ERROR - Message: #{conn_error.message}")
      logger.error("connects - ERROR - Backtrace: #{conn_error.backtrace}")
    end

    logger.debug("connects - conn = #{conn.inspect}")

    does_connect
  end


  # given a database, find its tables
  def get_table_names(db)
    db_adapter  = "postgresql"   # TODO: make this dynamic
    db_host     = db.host    # TODO: make this dynamic
    db_name     = db.name
    db_username = db.username
    db_password = db.password
    table_names = []


    # meta magic - create ActiveRecord class with connection different from default database
    active_record_class_name = "External_#{db_name}".camelize
    logger.debug("get_table_names - new class name = #{active_record_class_name}")
    dynamic_class = Class.new(ActiveRecord::Base) do
        self.abstract_class = true
    end
    Kernel.const_set active_record_class_name, dynamic_class


    # db connection
    begin
      conn_est    = dynamic_class.establish_connection(:adapter  => db_adapter, :host => db_host, :username => db_username, :password => db_password, :database => db_name)
      conn        = conn_est.connection   # actually check if connection works
      table_names = conn.tables.sort
    rescue => conn_error
      does_connect = false
      logger.error("get_table_names - ERROR - Message: #{conn_error.message}")
      logger.error("get_table_names - ERROR - Backtrace: #{conn_error.backtrace}")
    end

    logger.debug("##### get_table_names - table names = #{table_names.inspect} ")

    table_names
  end

end


