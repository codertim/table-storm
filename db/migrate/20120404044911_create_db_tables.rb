class CreateDbTables < ActiveRecord::Migration
  def change
    create_table :db_tables do |t|
      t.string :name
      t.references :db

      t.timestamps
    end
  end
end
