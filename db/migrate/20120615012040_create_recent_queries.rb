class CreateRecentQueries < ActiveRecord::Migration
  def change
    create_table :recent_queries do |t|
      t.references :db
      t.string :table_name

      t.timestamps
    end

    add_index :recent_queries, :db_id
  end
end

