class CreateDbs < ActiveRecord::Migration
  def change
    create_table :dbs do |t|
      t.string :type,     :limit => 20
      t.string :name,     :limit => 80
      t.string :username, :limit => 30
      t.string :password, :limit => 30
      t.string :host,     :limit => 80

      t.timestamps
    end
  end
end
